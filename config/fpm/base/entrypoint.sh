#!/bin/sh

# Enter data directory
cd /data

# Execute before scripts
for stage in base build dev custom; do
    for filename in /before_scripts/$stage/*.sh; do
        if [ -f $filename ]; then
            sh $filename
        fi
    done
done

exec php-fpm
