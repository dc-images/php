#!/bin/sh

# Update chdir directive in www.conf
# chdir = $php_fpm_chdir ?? "www"
[ -z "$PHP_FPM_CHDIR" ] && CHDIR="www" || CHDIR="$PHP_FPM_CHDIR"

sed -i -e "s/%chdir%/$CHDIR/g" \
    /usr/local/etc/php/php-fpm.d/www.conf \
    /usr/local/etc/php/php.ini