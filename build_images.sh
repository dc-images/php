#!/bin/bash

# What stage are we building
stage="$1"

# What stage do we extend
from="$2"

# Debug mode on/off (does not push images)
debug="$3"

if [ ! -z "$debug" ]; then
    CI_BUILD_REF_NAME="dev"
    CI_REGISTRY_IMAGE="registry.localhost/dc-images/php"
    
    PRIVATE_REPOSITORY_SSH_KEY="SSH Key"
    PRIVATE_REPOSITORY_SERVER_URL="Server url"
fi

# E.g: registry.gitlab.com/dc-images/php
vendor="$CI_REGISTRY_IMAGE"

# Temp folder for storing dockerfiles
[ -d tmp ] || mkdir tmp

# Prefix with slash if parent image was specified
# so image name is corect: 7.2-cli-alpine[/base]:v1.2.3
if [ ! -z "$from" ]; then
    from="/$from"
    buildTag=":$CI_BUILD_REF_NAME"
fi

for version in 7.2 7.3; do
    for type in cli fpm; do

        # create temp dockerfile
        path="tmp/$version-$type-$stage.Dockerfile"
        cp "dockerfiles/$type/$stage.Dockerfile" $path

        # e.g.: 7.2-cli-alpine
        tag="$version-$type-alpine"

        # e.g.: 7.2-cli-alpine[/base]:v1.2.3
        replace="$tag$from$buildTag"

        sed -i -e "s#\$VENDOR#$vendor#g" "$path"
        sed -i -e "s#\$IMAGE_TAG#$replace#g" "$path"

        IMAGE_NAME=$CI_REGISTRY_IMAGE/$tag/$stage
        IMAGE_TAG="$IMAGE_NAME:$CI_BUILD_REF_NAME"
        LATEST_TAG="$IMAGE_NAME:latest"

        if [ $CI_BUILD_REF_NAME == "master" ] || [ ! -z $debug ]; then
            docker build \
                -t $IMAGE_TAG \
                -f "$path" \
                --build-arg PRIVATE_REPOSITORY_SSH_KEY="$PRIVATE_REPOSITORY_SSH_KEY" \
                --build-arg PRIVATE_REPOSITORY_SERVER_URL="$PRIVATE_REPOSITORY_SERVER_URL" \
            .

            if [ -z $debug ]; then
                docker push $IMAGE_TAG
            fi
        else
            docker build \
                -t $IMAGE_TAG \
                -t $LATEST_TAG \
                -f "$path" \
                --build-arg PRIVATE_REPOSITORY_SSH_KEY="$PRIVATE_REPOSITORY_SSH_KEY" \
                --build-arg PRIVATE_REPOSITORY_SERVER_URL="$PRIVATE_REPOSITORY_SERVER_URL" \
            .

            docker push $IMAGE_TAG
            docker push $LATEST_TAG
        fi

    done
done
