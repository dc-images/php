FROM $VENDOR/$IMAGE_TAG

# Run stage 1
RUN \
    # Build dependencies
    apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
    # PHP Xdebug
    && pecl install xdebug-2.7.1 \
    && docker-php-ext-enable xdebug \
    # Cleanup
    && apk del .build-deps

COPY config/fpm/dev/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
COPY config/fpm/dev/before_scripts /before_scripts/dev