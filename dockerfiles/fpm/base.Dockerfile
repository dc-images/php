FROM php:$IMAGE_TAG

# Basic libraries
RUN \
    apk add --no-cache --virtual .build-deps \
        zlib-dev \
        $PHPIZE_DEPS \
    && apk add --no-cache --virtual .persistent-deps \
        libzip-dev \
    # PHP packages
    && docker-php-ext-install \
        zip \
        json \
        iconv \
        bcmath \
        opcache \
        calendar \
        pdo_mysql \
    # Redis
    && pecl install redis \
    && docker-php-ext-enable redis \
    # Setup working directory
    && mkdir \
        -m 0755 \
        -p /data \
    && chown -R www-data /data \
    && mkdir -p \
        /before_scripts/base \
        /before_scripts/build \
        /before_scripts/dev \
        /before_scripts/custom \
    # Cleanup
    && apk del --no-cache .build-deps

# GD Installation in separate layer
RUN \
    apk add --no-cache --virtual .build-deps \
    && apk add --no-cache \
      libpng-dev \
      freetype-dev \
      libjpeg-turbo-dev \
    && docker-php-ext-configure gd \
        --with-freetype-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
    && NPROC=$(getconf _NPROCESSORS_ONLN) \
    # PHP packages
    && docker-php-ext-install -j${NPROC} gd \
    # Cleanup
    && apk del --no-cache .build-deps


COPY config/fpm/base/entrypoint.sh /
COPY config/fpm/base/php.ini /usr/local/etc/php/php.ini
COPY config/fpm/base/www.conf /usr/local/etc/php/php-fpm.d/www.conf
COPY config/fpm/base/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY config/fpm/base/before_scripts /before_scripts/base/

WORKDIR /data

# Add a health check for the FPM server on port 9000
HEALTHCHECK --interval=60s --timeout=5s CMD SCRIPT_NAME=/ping SCRIPT_FILENAME=/ping REQUEST_METHOD=GET cgi-fcgi -bind -connect 127.0.0.1:9000

CMD [ "/entrypoint.sh" ]