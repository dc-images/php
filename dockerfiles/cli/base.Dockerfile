FROM php:$IMAGE_TAG

COPY config/cli/base/php.ini /usr/local/etc/php/php.ini

RUN \
    apk add --no-cache --virtual .build-deps \
        zlib-dev \
        $PHPIZE_DEPS \
    && apk add --no-cache --virtual .persistent-deps \
        libzip-dev \
    # PHP packages
    && docker-php-ext-install \
        zip \
        json \
        iconv \
    # Setup working directory
    && mkdir -p /data \
    # Cleanup
    && apk del --no-cache .build-deps

WORKDIR /data
